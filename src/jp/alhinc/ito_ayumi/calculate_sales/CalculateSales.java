package jp.alhinc.ito_ayumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	public static void main(String[] args) {

		BufferedReader br = null;
		Map<String,String> branchCodeAndNameMap = new HashMap<>();

		try {
			File branchListFile = new File(args[0],"branch.lst");//支店定義ファイル
			FileReader fr = new FileReader(branchListFile);
			br = new BufferedReader(fr);
			if (!branchListFile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}


			String line;
			while((line = br.readLine()) != null) {
				String[] codeAndName = line.split(",");
				branchCodeAndNameMap.put (codeAndName[0],codeAndName[1]);
				if( codeAndName.length != 2|| !codeAndName[0].matches("^[0-9]{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}


			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str){

					// 拡張子を指定する
					if (str.matches("[0-9]{8}.rcd")){
						return true;
					}else{
						return false;
					}
				}
			};


			File dir = new File(args[0]);//売上ファイル読み込み
			File[] filename = dir.listFiles(filter);


			Map<String,Long> resultsMap = new HashMap<>();// 集計結果を一時的に格納するマップ
			for(int i=0; i<filename.length; ++i) {
				br = new BufferedReader(new FileReader(filename[i]));

				String code;
				String sales;

				code = br.readLine();
				sales = br.readLine();

				if (resultsMap.containsKey(code) != true) {
					resultsMap.put(code,0L);
				}

				Long line2 = Long.parseLong(sales) + (resultsMap.get(code));
				resultsMap.put(code,line2);


				if(!line2.toString().matches("^[0-9]{1,10}$")){
					System.out.println("合計金額が10桁を超えました") ;
					return;
				}

				List<File> directryList = Arrays.asList(dir.listFiles());
				String index = "^[0-9]{8}.rcd$";
				List<File> searchedFileList = new ArrayList<>();
				List<Integer> nums = new ArrayList<>();

				for (File fl : directryList) {
					String fileName = fl.getName();
					if (fileName.matches(index) && fl.isFile()) {
						searchedFileList.add(fl);
						nums.add(Integer.parseInt(fileName.split("\\.")[0]));
					}
				}
				Collections.sort(nums);
				int min = nums.get(0);
				int max = nums.get(nums.size() - 1);
				if(min + nums.size() != max + 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}


				//////////支店コードの該当無ければエラー

				if(!branchCodeAndNameMap.containsKey(code)){
					System.out.println(filename[i].getName() + "の支店コードが不正です");
					return;
				}


				//////////売上ファイルの中身が3行以上あるときエラー
				//////////→3行目以降を読み込んでnullでないとき
				if((br.readLine()) != null) {
					System.out.println(filename[i].getName()+"のフォーマットが不正です");
					return;
				}


				File storeFile = new File(args[0],"branch.out");
				BufferedWriter bw = new BufferedWriter(new FileWriter(storeFile));
				for (String key : branchCodeAndNameMap.keySet()) {

					if (resultsMap.get(key) == null) {
						resultsMap.put (key,0L);
					}
					bw.write(key + "," + branchCodeAndNameMap.get(key) + "," + resultsMap.get(key));
				}
				bw.close();

			}


		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
}